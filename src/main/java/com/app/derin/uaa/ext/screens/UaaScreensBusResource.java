package com.app.derin.uaa.ext.screens;

import com.app.derin.uaa.service.dto.UaaScreensDTO;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UaaScreensBusResource {
    private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.screens.UaaScreensBusResource.class);
    private static final String ENTITY_NAME = "role";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaScreensBusService uaaScreensBusService;

    public UaaScreensBusResource(UaaScreensBusService uaaScreensBusService) {
        this.uaaScreensBusService = uaaScreensBusService;
    }

    @PostMapping("/ext/screens")
    public ResponseEntity<UaaScreensDTO> createScreen(@RequestBody UaaScreensDTO uaaScreensDTO) throws URISyntaxException {
        log.debug("REST request to save Screen : {}", uaaScreensDTO);
        if (uaaScreensDTO.getId() != null) {
            throw new BadRequestAlertException("A new course cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaScreensDTO result = uaaScreensBusService.save(uaaScreensDTO);
        return ResponseEntity.created(new URI("/api/screens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    @PutMapping("ext/screens")
    public ResponseEntity<UaaScreensDTO> updateScreen(@RequestBody UaaScreensDTO uaaScreensDTO) throws URISyntaxException {
        log.debug("REST request to update Screen : {}", uaaScreensDTO);
        if (uaaScreensDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaScreensDTO result = uaaScreensBusService.save(uaaScreensDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaScreensDTO.getId().toString()))
            .body(result);
    }
    @GetMapping("/ext/screens/all")
    public ResponseEntity<List<UaaScreensDTO>> getAllUaaScreens(Pageable pageable) {
        log.debug("REST request to get a page of Roles");
        Page<UaaScreensDTO> page = uaaScreensBusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/ext/screens/{id}")
    public ResponseEntity<UaaScreensDTO> getOneEntity(@PathVariable Long id) {
        UaaScreensDTO uaaScreensDTO = uaaScreensBusService.findOne(id);
        return ResponseEntity.ok().body(uaaScreensDTO);
    }
    @DeleteMapping("/ext/screens/{id}")
    public ResponseEntity<Void> deleteScreens(@PathVariable Long id) {
        log.debug("REST request to delete Screen : {}", id);
        uaaScreensBusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


}
