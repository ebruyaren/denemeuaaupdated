package com.app.derin.uaa.ext.screens;

import com.app.derin.uaa.domain.UaaScreens;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UaaScreensBusRepository {
    UaaScreens getOne(long id);

    Page<UaaScreens> getAll(Pageable pageable);

    UaaScreens save(UaaScreens uaaScreens);

    void deleteById(Long id);
}
