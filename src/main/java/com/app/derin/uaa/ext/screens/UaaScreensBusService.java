package com.app.derin.uaa.ext.screens;

import com.app.derin.uaa.domain.UaaScreens;
import com.app.derin.uaa.service.dto.UaaScreensDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UaaScreensBusService {
    UaaScreensDTO save(UaaScreensDTO uaaScreensDTO);
    Page<UaaScreensDTO> findAll(Pageable pageable);
    UaaScreensDTO findOne(Long id);

    void delete(Long id);

    UaaScreens getOne(long id);
}
