package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.domain.UaaUsers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UaaUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UaaUsersBusRepository {

    UaaUsers getOne(long id);

    Page<UaaUsers> getAll(Pageable pageable);

    UaaUsers save(UaaUsers uaaUsers);

    void deleteById(Long id);

    UaaUsers getRandomUser();

}
