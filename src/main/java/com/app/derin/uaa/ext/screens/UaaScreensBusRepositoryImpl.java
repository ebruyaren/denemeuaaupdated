package com.app.derin.uaa.ext.screens;

import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.domain.UaaScreens;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class UaaScreensBusRepositoryImpl implements UaaScreensBusRepository {
    @PersistenceContext
    private EntityManager entityManager;
    private List<String> screens;
    private Object Page;

    @Override
    public UaaScreens getOne(long id){
        String strQuery = "SELECT DISTINCT * FROM uaa_screens sc WHERE sc.id =:entity_id";
        Query query = entityManager.createNativeQuery(strQuery, UaaScreens.class);
        ((Query) query).setParameter("entity_id", id);
        return (UaaScreens) query.getResultStream().findFirst().orElse(null);
    }

    @Override
    public Page<UaaScreens> getAll(Pageable pageable) {
        String strQuery = "SELECT DISTINCT * FROM uaa_screens sc";
        Query query = entityManager.createNativeQuery(strQuery, UaaScreens.class);
        List<UaaScreens> screenList = query.getResultList();
        Page<UaaScreens> page = new PageImpl<>(screenList);
        Page p1 = new PageImpl<>(screenList, pageable, screenList.size());
        return page;
    }
    @Override
    public UaaScreens save(UaaScreens uaaScreens) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }


}
