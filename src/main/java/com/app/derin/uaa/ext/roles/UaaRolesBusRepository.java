package com.app.derin.uaa.ext.roles;

import com.app.derin.uaa.domain.UaaRoles;


import com.app.derin.uaa.domain.UaaUsers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UaaRolesBusRepository {

    UaaRoles getOne(long id);

    Page<UaaRoles> getAll(Pageable pageable);

    UaaRoles save(UaaRoles uaaRoles);

    void deleteById(Long id);


}
