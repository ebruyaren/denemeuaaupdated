package com.app.derin.uaa.ext.screens;

import com.app.derin.uaa.domain.UaaScreens;
import com.app.derin.uaa.service.dto.UaaScreensDTO;
import com.app.derin.uaa.service.mapper.UaaScreensMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UaaScreensBusServiceImpl implements UaaScreensBusService {
    private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.screens.UaaScreensBusServiceImpl.class);

    private final UaaScreensBusRepository uaaScreensBusRepository;
    private final UaaScreensMapper uaaScreensMapper;
    private java.util.List<UaaScreensDTO> List;

    public UaaScreensBusServiceImpl(UaaScreensBusRepository uaaScreensBusRepository, UaaScreensMapper uaaScreensMapper) {
        this.uaaScreensBusRepository = uaaScreensBusRepository;
        this.uaaScreensMapper = uaaScreensMapper;
    }

    @Override
    public UaaScreensDTO save(UaaScreensDTO uaaScreensDTO) {
        log.debug("Request to save UaaScreens : {}", uaaScreensDTO);
        UaaScreens uaaScreens = uaaScreensMapper.toEntity(uaaScreensDTO);
        uaaScreens = uaaScreensBusRepository.save(uaaScreens);
        return uaaScreensMapper.toDto(uaaScreens);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UaaScreensDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UaaScreens {}", pageable);
        return uaaScreensBusRepository.getAll(pageable)
            .map(uaaScreensMapper::toDto);
    }
    @Override
    public UaaScreensDTO findOne(Long id) {
        return uaaScreensMapper.toDto(uaaScreensBusRepository.getOne(id));
    }
    @Override
    public void delete(Long id) {
        uaaScreensBusRepository.deleteById(id);
    }

    @Override
    public UaaScreens getOne(long id) {
        return uaaScreensBusRepository.getOne(id);
    }

}
