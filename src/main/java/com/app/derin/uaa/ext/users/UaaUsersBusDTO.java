package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.domain.UaaScreens;
import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.ext.roles.UaaRolesBusRepository;
import com.app.derin.uaa.ext.screens.UaaScreensBusRepository;
import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import com.app.derin.uaa.service.mapper.UaaUsersMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class UaaUsersBusDTO extends UaaUsersDTO {

    private final UaaUsersBusRepository uaaUsersBusRepository;
    private final UaaRolesBusRepository uaaRolesBusRepository;
    private final UaaScreensBusRepository uaaScreensBusRepository;
    private final UaaUsersMapper uaaUsersMapper;

    public UaaUsersBusDTO(UaaUsersBusRepository uaaUsersBusRepository, UaaRolesBusRepository uaaRolesBusRepository, UaaScreensBusRepository uaaScreensBusRepository, UaaUsersMapper uaaUsersMapper) {
        this.uaaUsersBusRepository = uaaUsersBusRepository;
        this.uaaRolesBusRepository = uaaRolesBusRepository;
        this.uaaScreensBusRepository = uaaScreensBusRepository;
        this.uaaUsersMapper = uaaUsersMapper;
    }

    public UaaUsersBusRepository getUaaUsersBusRepository() {
        return uaaUsersBusRepository;
    }

    public UaaRolesBusRepository getUaaRolesBusRepository() {
        return uaaRolesBusRepository;
    }

    public UaaScreensBusRepository getUaaScreensBusRepository() {
        return uaaScreensBusRepository;
    }

    public UaaUsersMapper getUaaUsersMapper() {
        return uaaUsersMapper;
    }

//    @Override
//    ResponseEntity<UaaUsersBusDTO> findAllExtended(long id) {
//
//        UaaUsers uaaUsers = uaaUsersBusRepository.getOne(id);
//
//        List<UaaRoles> roleSet = (List<UaaRoles>) uaaRolesBusRepository.getAll(Pageable.unpaged());
//        Long[] ids = new Long[roleSet.size()];
//
//        List<UaaScreens> screensSet =  uaaScreensBusRepository.findRoleIds();
//
//        UaaUsersBusDTO uaaUsersBusDTO = new UaaUsersBusDTO(uaaUsersBusRepository, uaaRolesBusRepository, uaaScreensBusRepository, uaaUsersMapper);
//
//        uaaUsersBusDTO.setUaaUsersDTO(uaaUsersMapper.toDto(uaaUsers));
//
//        Set<UaaRolesDTO> rolesDTOs = new HashSet<>();
//
//        return null;
//    }
}
