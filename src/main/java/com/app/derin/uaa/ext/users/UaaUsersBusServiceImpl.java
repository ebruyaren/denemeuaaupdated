package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import com.app.derin.uaa.service.mapper.UaaUsersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UaaUsersBusServiceImpl implements UaaUsersBusService{
    private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.users.UaaUsersBusServiceImpl.class);

    private final UaaUsersBusRepository uaaUsersBusRepository;
    private final UaaUsersMapper uaaUsersMapper;

    public UaaUsersBusServiceImpl(UaaUsersBusRepository uaaUsersBusRepository, UaaUsersMapper uaaUsersMapper) {
        this.uaaUsersBusRepository = uaaUsersBusRepository;
        this.uaaUsersMapper = uaaUsersMapper;
    }


    @Override
    public UaaUsersDTO save(UaaUsersDTO uaaUsersDTO) {
        log.debug("Request to save UaaUsers : {}", uaaUsersDTO);
        UaaUsers uaaUsers = uaaUsersMapper.toEntity(uaaUsersDTO);
        uaaUsers = uaaUsersBusRepository.save(uaaUsers);
        return uaaUsersMapper.toDto(uaaUsers);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UaaUsersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UaaUsers {}", pageable);
//        return (Page<UaaRolesDTO>) uaaRolesBusRepository.getAll();
        return uaaUsersBusRepository.getAll(pageable)
            .map(uaaUsersMapper::toDto);
    }

    @Override
    public UaaUsersDTO findOne(Long id) {
        return uaaUsersMapper.toDto(uaaUsersBusRepository.getOne(id));
//        return uaaRolesBusRepository.getOne(id);
    }

    @Override
    public void delete(Long id) {
        uaaUsersBusRepository.deleteById(id);

    }

    @Override
    public UaaUsers getOne(long id) {

        return uaaUsersBusRepository.getOne(id);
    }
    @Override
    public UaaUsers getRandomUser() {
        return uaaUsersBusRepository.getRandomUser();
    }
}
