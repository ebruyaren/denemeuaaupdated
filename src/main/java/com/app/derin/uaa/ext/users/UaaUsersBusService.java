package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UaaUsersBusService {
    UaaUsersDTO save(UaaUsersDTO uaaUsersDTO);
    Page<UaaUsersDTO> findAll(Pageable pageable);
    UaaUsersDTO findOne(Long id);
    void delete(Long id);
    UaaUsers getOne(long id);
    UaaUsers getRandomUser();
}
