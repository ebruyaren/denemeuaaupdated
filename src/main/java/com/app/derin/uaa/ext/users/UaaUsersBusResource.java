package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UaaUsersBusResource {
    private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.users.UaaUsersBusResource.class);
    private static final String ENTITY_NAME = "role";
    private static final String str = "role2";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaUsersBusService uaaUsersBusService;

    public UaaUsersBusResource(UaaUsersBusService uaaUsersBusService) {
        this.uaaUsersBusService = uaaUsersBusService;
    }

    @PostMapping("/ext/users")
    public ResponseEntity<UaaUsersDTO> createScreen(@RequestBody UaaUsersDTO uaaUsersDTO) throws URISyntaxException {
        log.debug("REST request to save User : {}", uaaUsersDTO);
        if (uaaUsersDTO.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaUsersDTO result = uaaUsersBusService.save(uaaUsersDTO);
        return ResponseEntity.created(new URI("/api/users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    @PutMapping("ext/users")
    public ResponseEntity<UaaUsersDTO> updateScreen(@RequestBody UaaUsersDTO uaaUsersDTO) throws URISyntaxException {
        log.debug("REST request to update User : {}", uaaUsersDTO);
        if (uaaUsersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaUsersDTO result = uaaUsersBusService.save(uaaUsersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaUsersDTO.getId().toString()))
            .body(result);
    }
    @GetMapping("/ext/users/all")
    public ResponseEntity<List<UaaUsersDTO>> getAllUaaUsers(Pageable pageable) {
        log.debug("REST request to get a page of Users");
        Page<UaaUsersDTO> page = uaaUsersBusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/ext/users/{id}")
    public ResponseEntity<UaaUsersDTO> getOneEntity(@PathVariable Long id) {
        UaaUsersDTO uaaUsersDTO = uaaUsersBusService.findOne(id);
        return ResponseEntity.ok().body(uaaUsersDTO);
    }
    @DeleteMapping("/ext/users/{id}")
    public ResponseEntity<Void> deleteUsers(@PathVariable Long id) {
        log.debug("REST request to delete User : {}", id);
        uaaUsersBusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("ext/users/randomUser")
    public ResponseEntity<UaaUsers> getRandomUser() {

        UaaUsers uaaUsersDTO = uaaUsersBusService.getRandomUser();
        return ResponseEntity.ok().body(uaaUsersDTO);
    }
}
