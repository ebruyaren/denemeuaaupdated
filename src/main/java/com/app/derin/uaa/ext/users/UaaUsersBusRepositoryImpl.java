package com.app.derin.uaa.ext.users;


import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.domain.UaaUsers;
import org.bouncycastle.asn1.ua.UAObjectIdentifiers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public  class UaaUsersBusRepositoryImpl implements UaaUsersBusRepository {

    @PersistenceContext
    private EntityManager entityManager;
    private List<String> roles;
    private Object Page;


    @Override
    public UaaUsers getOne(long id){
        String strQuery = "SELECT DISTINCT * FROM uaa_users u WHERE u.id =:entity_id";
        Query query = entityManager.createNativeQuery(strQuery, UaaUsers.class);
        ((Query) query).setParameter("entity_id", id);
//        return (UaaRoles) query.getResultStream().findFirst().orElse(null);
        return (UaaUsers) query.getResultStream().findFirst().orElse(null);
    }

    @Override
    public Page<UaaUsers> getAll(Pageable pageable) {
        String strQuery = "SELECT DISTINCT * FROM uaa_users u";
        Query query = entityManager.createNativeQuery(strQuery, UaaUsers.class);
        List<UaaUsers> rolesList = query.getResultList();
        Page<UaaUsers> page = new PageImpl<>(rolesList);
        Page p1 = new PageImpl<>(rolesList, pageable, rolesList.size());
        return page;
    }

    @Override
    public UaaUsers save(UaaUsers uaaUsers) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
        public UaaUsers getRandomUser() {
        String strQuery = "SELECT DISTINCT * FROM uaa_users users";
        Query query = entityManager.createNativeQuery(strQuery, UaaUsers.class);
        List<UaaUsers> usersList = query.getResultList();
        Random rand = new Random();
        UaaUsers user = (usersList.get(rand.nextInt(usersList.size())));
        return user;
    }

}
