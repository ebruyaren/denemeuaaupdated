package com.app.derin.uaa.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UaaScreens.
 */
@Entity
@Table(name = "uaa_screens")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UaaScreens implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "screen_name")
    private String screenName;

    @ManyToMany(mappedBy = "screens")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<UaaRoles> roles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScreenName() {
        return screenName;
    }

    public UaaScreens screenName(String screenName) {
        this.screenName = screenName;
        return this;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Set<UaaRoles> getRoles() {
        return roles;
    }

    public UaaScreens roles(Set<UaaRoles> uaaRoles) {
        this.roles = uaaRoles;
        return this;
    }

    public UaaScreens addRoles(UaaRoles uaaRoles) {
        this.roles.add(uaaRoles);
        uaaRoles.getScreens().add(this);
        return this;
    }

    public UaaScreens removeRoles(UaaRoles uaaRoles) {
        this.roles.remove(uaaRoles);
        uaaRoles.getScreens().remove(this);
        return this;
    }

    public void setRoles(Set<UaaRoles> uaaRoles) {
        this.roles = uaaRoles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UaaScreens)) {
            return false;
        }
        return id != null && id.equals(((UaaScreens) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UaaScreens{" +
            "id=" + getId() +
            ", screenName='" + getScreenName() + "'" +
            "}";
    }
}
