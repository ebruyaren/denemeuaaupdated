package com.app.derin.uaa.service;

import com.app.derin.uaa.service.dto.UaaRolesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.app.derin.uaa.domain.UaaRoles}.
 */
public interface UaaRolesService {

    /**
     * Save a uaaRoles.
     *
     * @param uaaRolesDTO the entity to save.
     * @return the persisted entity.
     */
    UaaRolesDTO save(UaaRolesDTO uaaRolesDTO);

    /**
     * Get all the uaaRoles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UaaRolesDTO> findAll(Pageable pageable);

    /**
     * Get all the uaaRoles with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<UaaRolesDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" uaaRoles.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UaaRolesDTO> findOne(Long id);

    /**
     * Delete the "id" uaaRoles.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
