package com.app.derin.uaa.service.mapper;

import com.app.derin.uaa.domain.*;
import com.app.derin.uaa.service.dto.UaaScreensDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UaaScreens} and its DTO {@link UaaScreensDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UaaScreensMapper extends EntityMapper<UaaScreensDTO, UaaScreens> {


    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "removeRoles", ignore = true)
    UaaScreens toEntity(UaaScreensDTO uaaScreensDTO);

    default UaaScreens fromId(Long id) {
        if (id == null) {
            return null;
        }
        UaaScreens uaaScreens = new UaaScreens();
        uaaScreens.setId(id);
        return uaaScreens;
    }
}
