package com.app.derin.uaa.service.mapper;

import com.app.derin.uaa.domain.*;
import com.app.derin.uaa.service.dto.UaaUsersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UaaUsers} and its DTO {@link UaaUsersDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UaaUsersMapper extends EntityMapper<UaaUsersDTO, UaaUsers> {


    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "removeRoles", ignore = true)
    UaaUsers toEntity(UaaUsersDTO uaaUsersDTO);

    default UaaUsers fromId(Long id) {
        if (id == null) {
            return null;
        }
        UaaUsers uaaUsers = new UaaUsers();
        uaaUsers.setId(id);
        return uaaUsers;
    }
}
