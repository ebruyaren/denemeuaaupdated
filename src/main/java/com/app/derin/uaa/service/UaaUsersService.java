package com.app.derin.uaa.service;

import com.app.derin.uaa.service.dto.UaaUsersDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.app.derin.uaa.domain.UaaUsers}.
 */
public interface UaaUsersService {

    /**
     * Save a uaaUsers.
     *
     * @param uaaUsersDTO the entity to save.
     * @return the persisted entity.
     */
    UaaUsersDTO save(UaaUsersDTO uaaUsersDTO);

    /**
     * Get all the uaaUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UaaUsersDTO> findAll(Pageable pageable);


    /**
     * Get the "id" uaaUsers.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UaaUsersDTO> findOne(Long id);

    /**
     * Delete the "id" uaaUsers.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
