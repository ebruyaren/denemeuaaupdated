package com.app.derin.uaa.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.app.derin.uaa.domain.UaaUsers} entity.
 */
public class UaaUsersDTO implements Serializable {

    private Long id;

    private String userName;

    private String userMail;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UaaUsersDTO uaaUsersDTO = (UaaUsersDTO) o;
        if (uaaUsersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), uaaUsersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UaaUsersDTO{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", userMail='" + getUserMail() + "'" +
            "}";
    }
}
