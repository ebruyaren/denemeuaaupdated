package com.app.derin.uaa.repository;

import com.app.derin.uaa.domain.UaaScreens;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UaaScreens entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UaaScreensRepository extends JpaRepository<UaaScreens, Long> {

}
