package com.app.derin.uaa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaRolesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaRoles.class);
        UaaRoles uaaRoles1 = new UaaRoles();
        uaaRoles1.setId(1L);
        UaaRoles uaaRoles2 = new UaaRoles();
        uaaRoles2.setId(uaaRoles1.getId());
        assertThat(uaaRoles1).isEqualTo(uaaRoles2);
        uaaRoles2.setId(2L);
        assertThat(uaaRoles1).isNotEqualTo(uaaRoles2);
        uaaRoles1.setId(null);
        assertThat(uaaRoles1).isNotEqualTo(uaaRoles2);
    }
}
