package com.app.derin.uaa.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class UaaUsersMapperTest {

    private UaaUsersMapper uaaUsersMapper;

    @BeforeEach
    public void setUp() {
        uaaUsersMapper = new UaaUsersMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(uaaUsersMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uaaUsersMapper.fromId(null)).isNull();
    }
}
